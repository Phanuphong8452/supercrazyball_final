﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : MonoBehaviour
{
    public float MovementSpeed, Jumpforce = 1;

    private Rigidbody _rigidbody;
    
    private void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

    
    private void FixedUpdate()
    {
        var movement = Input.GetAxis("Horizontal");
        var movement1 = Input.GetAxis("Vertical");
        
        transform.position += new Vector3(-movement,0,-movement1) * Time.deltaTime * MovementSpeed;
        
        if (Input.GetButtonDown("Jump"))
        {
            _rigidbody.AddForce(new Vector3(0,Jumpforce,0), ForceMode.Impulse);
        }
    }
}
